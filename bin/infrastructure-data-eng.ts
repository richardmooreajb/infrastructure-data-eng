#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { InfrastructureDataEngStack } from '../lib/infrastructure-data-eng-stack';

const app = new cdk.App();

new InfrastructureDataEngStack(app, 'InfrastructureDataEngStack', {
  env: { region: 'eu-west-1', account: '390908342138' }
});

app.synth();