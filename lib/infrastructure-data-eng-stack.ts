import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { CodePipeline, CodePipelineSource, ShellStep, ManualApprovalStep } from 'aws-cdk-lib/pipelines';
import { AppStage } from './app-stage';
export class InfrastructureDataEngStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const repo = 'richardmooreajb/infrastructure-data-eng'

    const pipeline = new CodePipeline(this, 'Pipeline', {
      pipelineName: 'InfrastructureDataEngineeringCommonPipeline',
      synth: new ShellStep('Synth', {
        input: CodePipelineSource.connection(repo, 'master', {
          connectionArn: 'arn:aws:codestar-connections:eu-west-1:390908342138:connection/dea33a11-b2b3-44be-a336-aa0d1359ac25',
        }),
        commands: ['npm ci', 'npm run build', 'npx cdk synth'],
      }),
    });

  }
}